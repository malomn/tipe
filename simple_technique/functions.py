from math import sqrt


def norm_inferior_to(center, radius):
    """
    Implementation of the Manhattan norm 
    """
    return abs(center[0]) + abs(center[1]) <= radius


def dist(a, b):
    d = abs(a - b)
    d = sum(d**2)
    return sqrt(d)


def gen(a, x, b, y):
    for i in range(a, x):
        for j in range(b, y):
            yield i, j


def translate(leftMin, leftMax, rightMin, rightMax):
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin
    return lambda value : rightMin + (float(value - leftMin) / float(leftSpan))*rightSpan


def kernel(point, size):
    for i in range(point[0] - size, point[0] + size + 1):
        for j in range(point[1] - size, point[1] + size + 1):
            if norm_inferior_to((point[0] - i, point[1] - j), size):
                yield i, j


def kern_color_diff(a, b):
    """
    Here, a and b are complex arrays constructed as following:
        b.shape = a.shape = (n, 3) where n is an int greater than 0
    """
    s = 0
    for i, j in zip(a, b):
        s += sum(abs(i - j)**2)
    
    return s
