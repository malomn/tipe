import numpy as np
import cv2
from time import time
from skimage import io
from pathlib import Path

debug_folder = 'debug/'
Path(debug_folder).mkdir(parents=True, exist_ok=True)


def gen(a, x, b, y):
    for i in range(a, x):
        for j in range(b, y):
            yield i, j


def simple_disparity(left, right, factor=10, square=0, debug=True):
    left, right = cv2.resize(left, (left.shape[1] // factor, left.shape[0] // factor)), \
                  cv2.resize(right, (right.shape[1] // factor, right.shape[0] // factor))
    print(left.shape)

    nb_pixels = (left.shape[0] - 2*square)*(left.shape[1] - 2*square)

    # first_loop = True
    result = np.zeros(left.shape[:2] + (1,), dtype=float)

    # For each pixel of an image
    z = 0
    threshold = 1500
    print("Threshold is currently {}".format(threshold))
    t_init = time()

    for x, y in gen(square, left.shape[0] - square, square, left.shape[1] - square):

        # We run through the corresponding line on the other picture.
        column = y
        for b in range(square, right.shape[0] - square):

            d = np.sum(abs(left[x - square:x + square + 1, y - square:y + square + 1] - right[x - square:x + square + 1,
                                                                                        b - square:b + square + 1]) ** 2)
            d = d / (2 * square + 1) ** 2

            if d < threshold:
                column = b
                break

        # We compute the distance
        distance = abs(column - y)
        result[x, y] = distance

        if z % 1000 == 0 and debug:
            percent = z * 100 / nb_pixels
            t_end = 100 * (time() - t_init) / percent if percent != 0 else 0
            t_end = int(t_end - time() + t_init)
            sec = str(t_end % 60)
            sec = sec if len(sec) == 2 else '0' + sec
            minu = str(t_end // 60)
            minu = minu if len(minu) == 2 else '0' + minu
            print('{} / {} - {} % - {}:{}. '.format(z, nb_pixels, int(percent), minu, sec))
        z += 1

    if debug:
        result = result / np.amax(result) * 255
        result = result.astype('uint8')
        print(np.mean(result))
        io.imsave(debug_folder + "simple_algorithm_result.png", result)

    return result


if __name__ == '__main__':
    factor = 10
    l = cv2.imread('left.png').astype(float)
    r = cv2.imread('right.png').astype(float)
    simple_disparity(l, r, factor=1, square=4)
