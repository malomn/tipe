from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io
import numpy as np
from sklearn.cluster import OPTICS
# from hdbscan import HDBSCAN

image = img_as_float(io.imread(fname="disparity.png"))

### SLIC ###
numSegments = 1000
segments = slic(image, n_segments=numSegments, sigma=5)

io.imsave('slic.png', (mark_boundaries(image, segments)))

img = image[:, :, 0]
# Collect super-pixels and average the colors in each one
superpixels = []
groups = []
for g in np.unique(segments):
	# group = [[x, y, image[x, y]] for x in range(image.shape[0]) for y in range(image.shape[1]) if segments[x, y] == g]
	group = np.where(segments == g)
	group = [[x, y, img[x, y]] for (x, y) in zip(group[0], group[1])]
	# group = [[p, image[p]] for p in zip(group[0], group[1])]
	groups.append(group)
	group = np.mean(group, axis=0)
	superpixels.append(group)

superpixels, groups = np.array(superpixels), np.array(groups)

# Removing black super-pixels
remove_position = np.argwhere(superpixels[:, 2] < 0.1).flatten()

superpixels = np.delete(superpixels, remove_position, axis=0)
groups = np.delete(groups, remove_position, axis=0)

### OPTICS ###
clustering = OPTICS(min_samples=5, metric='euclidean').fit(superpixels)

### HDBSCAN ###
# clustering = HDBSCAN(metric='euclidean', min_cluster_size=5, min_samples=1).fit(superpixels)

print(clustering.labels_)

# Displaying the three biggest groups
colors = [[i, i, 1] for i in np.linspace(0, 1.0, 6)]

ids = np.unique(clustering.labels_)
ids = sorted(ids, key=lambda x: np.count_nonzero(clustering.labels_ == x), reverse=True)
ids.remove(-1)
newimg = image.copy()

for i in range(min(len(colors), len(ids))):
	# forming the index group
	index_group = []
	for j in np.argwhere(clustering.labels_ == ids[i]).flatten():
		index_group += groups[j]

	# Setting the pixels white when in group
	for j in index_group:
		newimg[j[0], j[1]] = colors[i]

io.imsave('OPTICS.png', newimg)
