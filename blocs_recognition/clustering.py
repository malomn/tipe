from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage import io
import numpy as np
from sklearn.cluster import OPTICS
from hdbscan import HDBSCAN
from pathlib import Path

debug_folder = 'debug/'


def to_uint8(array):
    out = array*255
    return out.astype('uint8')


def mapper(left_min, left_max, right_min, right_max):
    left_span = left_max - left_min
    right_span = right_max - right_min
    return lambda value: right_min + (float(value - left_min) / float(left_span))*right_span


def slic_segmentation(distance_map, num_segments=1000, threshold=0.2, debug=True):
    Path(debug_folder).mkdir(parents=True, exist_ok=True)

    # Checking the shape of the array
    distance_map = np.array(distance_map)
    if len(distance_map.shape) != 2:
        raise ValueError("Initial array should have a (m, n) shape. The shape is currently {}."
                         .format(distance_map.shape))

    # Setting all the values in a [0, 1] interval.
    f = np.vectorize(mapper(np.amin(distance_map), np.amax(distance_map), 0.0, 1.0))
    img = f(distance_map)
    img = abs(img - 1)

    # SLIC SEGMENTATION
    new_img = np.zeros(img.shape + (3,))
    for i in range(3):
        new_img[:, :, i] = img.copy()

    segments = slic(new_img, n_segments=num_segments, sigma=5)
    if debug:
        io.imsave(debug_folder + 'slic_segmentation.png', to_uint8(mark_boundaries(new_img, segments)))
        # Saving a colored slic segmentation for visualisation purposes
        slic_visual = new_img.copy()
        for i in np.unique(segments):
            group = np.argwhere(segments == i)
            colors = []
            for p in group:
                colors.append(slic_visual[p[0], p[1]])
            colors = np.array(colors)
            color = np.mean(colors, axis=0)

            color = (1, 0, 0) if np.mean(color) < threshold else color
            for p in group:
                slic_visual[p[0], p[1]] = color

        io.imsave(debug_folder + "slic_colored.png", to_uint8(slic_visual))

        return segments, img, new_img


def clusters(distance_map, segments, img, new_img, method='optics', min_samples=5, threshold=0.1, debug=True):
    """
    Return the clusters recognized, first being the biggest one.
    :param distance_map: map of the distances between each pixel and the cameras
    :param segments: result of slic algorithm
    :param img: result of slic algorithm
    :param new_img: result of slic algorithm
    :param method: 'optics' or 'hdbscan'
    :param min_samples: minimum of samples in an OPTICS cluster
    :param threshold: pixels under that value will be considered as too far to be interesting
    :param debug: if True, saves pictures at different steps of the process in debug/
    :return: sorted list of blocs, first being the biggest
    """
    # Collect super-pixels and average the colors in each one
    superpixels, groups = [], []
    for g in np.unique(segments):
        group = np.where(segments == g)
        group = [[x, y, img[x, y]] for (x, y) in zip(group[0], group[1])]
        groups.append(group)
        group = np.mean(group, axis=0)
        superpixels.append(group)

    superpixels, groups = np.array(superpixels), np.array(groups)

    # Removing black super-pixels
    remove_position = np.argwhere(superpixels[:, 2] < threshold).flatten()

    superpixels = np.delete(superpixels, remove_position, axis=0)

    if debug:
        np.save(debug_folder + 'superpixels', superpixels)

    groups = np.delete(groups, remove_position, axis=0)

    # ACTUAL CLUSTERING
    method = method.upper()
    if method == "OPTICS":
        clustering = OPTICS(min_samples=min_samples, metric='euclidean').fit(superpixels)
    elif method == "HDBSCAN":
        clustering = HDBSCAN(metric='euclidean', min_cluster_size=min_samples, min_samples=1).fit(superpixels)
    else:
        raise ValueError("There are only two methods available: hdbscan and optics")

    if debug:
        print(clustering.labels_)

    ids = np.unique(clustering.labels_)
    ids = sorted(ids, key=lambda x: np.count_nonzero(clustering.labels_ == x), reverse=True)
    if -1 in ids:
        ids.remove(-1)

    # Displaying the clusters
    if debug:
        colors = [[i, i, 1] for i in np.linspace(0, 1.0, 6)]

        for i in range(min(len(colors), len(ids))):
            # forming the index group
            index_group = []
            for j in np.argwhere(clustering.labels_ == ids[i]).flatten():
                index_group += groups[j]

            # Setting the pixels of the group in the according color
            for j in index_group:
                new_img[j[0], j[1]] = colors[i]

        io.imsave(debug_folder + method.upper() + '_' + str(min_samples) + '.png', to_uint8(new_img))

    # Output data is like this: [(number of points, average value, center coordinates), ...]
    output = []
    for i in ids:
        indexes = []
        for j in np.argwhere(clustering.labels_ == i).flatten():
            indexes += groups[j]
        indexes = [x[:2] for x in indexes]

        distances = np.array([distance_map[p[0], p[1]] for p in indexes if img[p[0], p[1]] >= threshold])
        output.append((len(indexes), np.median(distances), tuple(np.mean(indexes, axis=0))))
    return output


if __name__ == "__main__":
    from skimage.util import img_as_float
    image = img_as_float(io.imread(fname="disparity.png"))
    image = image[:, :, 0]*10
    print(clusters(image))
