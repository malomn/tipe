from blocs_recognition.clustering import clusters, slic_segmentation
from stereo_match import stereoSGBM
import numpy as np
import cv2 as cv

disparity_value = "10" # 05, 10, 15...
distance_value = "200" # 050, 100...

imgL = cv.imread("images_exp_undistored/" + disparity_value + "-" + distance_value + "-g.jpg", 0)
imgR = cv.imread("images_exp_undistored/" + disparity_value + "-" + distance_value + "-d.jpg", 0)
Q = np.loadtxt("calibration/Q-" + disparity_value + ".txt")

disparity, Q = stereoSGBM(imgL, imgR, False, Q, int(disparity_value))

distance_map = cv.reprojectImageTo3D(disparity, Q)[:, :, 2]
print(distance_map[347][437])
np.save('distance_map', distance_map)

seuil = 0.4
segments, img, new_img = slic_segmentation(distance_map, num_segments=1000, threshold=seuil, debug=True)

blocs = clusters(distance_map, segments, img, new_img, method='optics', min_samples=9, threshold=seuil, debug=True)
print(blocs)
