import cv2
import pickle
import numpy
import glob

images = glob.glob('../images_exp/*.jpg')

# Save info in a pickle file
with open("calib", "rb") as f:
    ret, mtx, dist, rvecs, tvecs = pickle.load(f)

for fname in images:
    img = cv2.imread(fname)
    img = cv2.resize(img, (img.shape[1] // 5, img.shape[0] // 5))
    h, w = img.shape[:2]
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))

    # undistort
    dst = cv2.undistort(img, mtx, dist, None, newcameramtx)

    # crop the image
    x, y, w, h = roi
    dst = dst[y:y + h, x:x + w]

    #fname = fname.replace('.jpg')
    fname = fname.replace('../images_exp/', '../images_exp_undistored/')
    print(fname)
    cv2.imwrite('../images_exp_undistored/' + fname, dst)
