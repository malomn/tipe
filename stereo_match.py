from __future__ import print_function

import numpy as np
import cv2 as cv

def stereoSGBM(imgL, imgR, resize, Q, disparity):
    '''
    imgL, imgR : images gauches et droite
    resize : booléen
    Q : matrice relavive à la disparité
    disparity : disparité en cm
    return : carte de disparité et matrice Q
    '''
    maxDisparities = {
    "5": 112,
    "10": 220,
    "20": 180
    }

    print('loading images...')

    if resize:
        imgL = cv.resize(imgL, (int(imgL.shape[1]//2.3), int(imgL.shape[0]//2.3)))
        imgR = cv.resize(imgR, (int(imgR.shape[1]//2.3), int(imgR.shape[0]//2.3)))

    h, w = imgL.shape[:2]
    print("h, w : ", h, w)

    Q[2][3] = 18 * (w/22.3) # Focal (en px) dépend de la largeur de l'image (22.3 : largeur du capteur).

    window_size = 3
    min_disp = 16
    max_disp = maxDisparities.get(str(disparity))
    num_disp = max_disp-min_disp
    stereo = cv.StereoSGBM_create(minDisparity = min_disp,
        numDisparities = num_disp,
        blockSize = 15,
        P1 = 8*3*window_size**2,
        P2 = 32*3*window_size**2,
        disp12MaxDiff = 1, # 1
        uniquenessRatio = 10,
        speckleWindowSize = 100,
        speckleRange = 32
    )

    print('calcul de la disparité')
    disp = stereo.compute(imgL, imgR).astype(np.float32) / 16.0
    print('Matrice Q : ', Q)
    cv.imshow('image gauche', imgL)
    cv.imshow('carte disparite', (disp-min_disp)/num_disp)
    cv.waitKey()

    return disp, Q
