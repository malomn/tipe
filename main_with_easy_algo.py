from blocs_recognition.clustering import clusters, slic_segmentation, mapper
from stereo_match import stereoSGBM
from simple_technique.simple_algo import simple_disparity
import numpy as np
import cv2 as cv

images = ("images_exp_undistored/20-100-g.jpg",
          "images_exp_undistored/20-100-d.jpg")

### OPENCV ###
imgL = cv.imread(images[0], 0)
imgR = cv.imread(images[1], 0)
Q = np.loadtxt("calibration/Q-10.txt")

opencv_disparity, Q = stereoSGBM(imgL, imgR, False, Q, 20)

### SIMPLE ALGORITHM ###
imgL = cv.imread(images[0]).astype(float)
imgR = cv.imread(images[1]).astype(float)
easy_disparity = simple_disparity(imgL, imgR, factor=5, square=0, debug=True)
# Adapt the disparity to match the dimensions of the opencv_disparity
easy_disparity = cv.resize(easy_disparity, (opencv_disparity.shape[1], opencv_disparity.shape[0]))
f = np.vectorize(mapper(np.amin(easy_disparity), np.amax(easy_disparity),
                        np.amin(opencv_disparity), np.amax(opencv_disparity)))
easy_disparity = f(easy_disparity).astype(np.float32)
easy_disparity = abs(255 - easy_disparity)

easy_or_opencv = 'opencv'

if easy_or_opencv == 'easy':
    distance_map = cv.reprojectImageTo3D(easy_disparity, Q)[:, :, 2]

    seuil = 0.4
    segments, img, new_img = slic_segmentation(distance_map, num_segments=1000, threshold=seuil, debug=True)
    blocs = clusters(distance_map, segments, img, new_img, method='optics', min_samples=3, threshold=seuil, debug=True)

elif easy_or_opencv == 'opencv':
    distance_map = cv.reprojectImageTo3D(opencv_disparity, Q)[:, :, 2]

    seuil = 0.4
    segments, img, new_img = slic_segmentation(distance_map, num_segments=1000, threshold=seuil, debug=True)
    blocs = clusters(distance_map, segments, img, new_img, method='optics', min_samples=9, threshold=seuil, debug=True)

print(blocs)
